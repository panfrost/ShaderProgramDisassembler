#pragma once

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

void DisassembleBifrost(uint8_t* instBlob, size_t size);
void DisassembleMidgard(uint8_t* instBlob, size_t size);

#ifdef __cplusplus
};
#endif
